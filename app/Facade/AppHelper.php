<?php
namespace App\Facade;
use Illuminate\Support\Facades\Facade;


class AppHelper extends Facade
{
    protected static function getFacadeAccessor() { return 'apphelper'; }
}