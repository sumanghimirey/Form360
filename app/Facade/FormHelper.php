<?php
namespace App\Facade;
use Illuminate\Support\Facades\Facade;


class FormHelper extends Facade
{
    protected static function getFacadeAccessor() { return 'formhelper'; }
}