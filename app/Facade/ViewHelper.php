<?php
namespace App\Facade;
use Illuminate\Support\Facades\Facade;


class ViewHelper extends Facade
{
    protected static function getFacadeAccessor() { return 'viewhelper'; }
}