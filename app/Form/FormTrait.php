<?php
namespace App\Form;


use App\Model\Form;
use App\Model\Question;
use DB;

trait FormTrait {


    public  function getFormDetail(){

        $form_detail= Form::where('status',1)->first();

        return $form_detail;
    }


    public function getFormQuestionById($id){

        $form_question=DB::table('forms')
            ->select(
                'q.id','q.title','q.type','q.name')
            ->leftJoin(
                'form_questions as fq' ,'fq.form_id','=','forms.id'
            )->leftJoin(
                'questions as q', 'q.id','=','fq.question_id'
            )->where('forms.id',$id)
             ->where('forms.status','1')
             ->where('q.status','1')
             ->get();

        return $form_question;

    }

    public function getAllQuestion(){

        $questions=Question::where('status',1)->all();

        return $questions;
    }

    public function getQuestionByType($type){

        $questionByType=Question::where('status',1)->where('type',$type);

        return $questionByType;
    }


    public  function fromResponses(){
        $form_response=DB::table('form_responses')->select(
            'f.title as form_title','q.title as question_title','form_responses.form_responses')
            ->leftJoin('form_questions as fq','fq.id','=','form_responses.form_id')
            ->leftJoin('forms as f','f.id','=','form_responses.form_id')
            ->leftJoin('questions as q','q.id','=','form_responses.question_id');

        return $form_response;

    }


}