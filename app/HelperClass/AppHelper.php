<?php
namespace App\HelperClass;


use App\Model\SiteConfiguration;
use App\Model\Currency;

class AppHelper
{
    public function getTransPathFromViewPath($view_path)
    {
        $tmp = explode('.', $view_path); // admin , user, index
        array_pop($tmp);

        $tmp = implode('/', $tmp);
        return $tmp.'/general.';

    }



    
    public function changeToKeyValArray($data, $key, $value)
    {
        $tmp = [];
        foreach ($data as $item) {

            $tmp[$item->$key] = $item->$value;
        }

        return $tmp;
    }

    public function makeFolderIfNotExist($path)
    {
        if (!file_exists($path)) {
            // create folder
            mkdir($path);
        }
    }

}