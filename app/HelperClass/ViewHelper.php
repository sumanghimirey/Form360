<?php
namespace App\HelperClass;

use DB;

class ViewHelper
{
    public function getAdminRoute($route)
    {
        return 'admin.'.$route;
    }

    public function showValidationMessage($errors, $field)
    {
        if ($errors->has($field))
            echo '<span class="help-block">'.$errors->first($field).'</span>';

    }

    public function ShowValidationClass($errors, $field)
    {
        if ($errors->has($field))
            return 'has-error';
    }

    public function getFormValue($key, $data = '')
    {
        if (old($key))
            echo old($key);

        if (!empty($data)) {
            echo $data['row']->$key;
        }

        echo '';

    }

    public function getFormStatus($action, $data = '')
    {

        /*
         *
        1 && 1 = 1
        1 && 0 = 0
        0 && 1 = 0
        0 && 0 = 0

        1 || 1 = 1
        1 || 0 = 1
        0 || 1 = 1
        0 || 0 = 0

        */

        $chk_1 = 'checked';
        $chk_2 = '';

        if (!empty($data) && $data['row']->status == 0) {

            $chk_1 = '';
            $chk_2 = 'checked';
        }

        if (old('status')) {

            if (old('status') == 'inactive') {

                $chk_1 = '';
                $chk_2 = 'checked';

            } else {

                $chk_1 = 'checked';
                $chk_2 = '';

            }
        }

        if ($action == 'active')
            return $chk_1;

        return $chk_2;
    }
    public function getCheckboxStatus($data="",$key)
    {
        $checked='';
        //dd($data->is_featured);
        if(!empty($data) && $data['row']->$key)
        {
            $checked='checked';
        }
        if(old($key))
        {
            //the checkbox is checked return the value
            $checked='checked';
            
        }
        return $checked;
        
    }


    public function getDefaultValue($value,$data="")
    {
        //get the default selected status
        $defaultYes="";
        $defaultNo="selected";

        if (!empty($data) && $data['row']->default == 1) {

            $defaultYes="selected";
            $defaultNo="";
        }
        if (old('default')) {

            if (old('default') == 'yes') {

                $defaultYes="selected";
                $defaultNo="";

            } else {

                $defaultYes="";
                $defaultNo="selected";

            }
        }
        if($value=="yes")
        return $defaultYes;
        return $defaultNo;
    }


    public function getImagePath($image_for)
    {
        return config('act360.asset.path.image').config('act360.asset.image_folder.'.$image_for).'/';
    }

    

}