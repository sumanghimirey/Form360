<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use AppHelper;

class AdminBaseController extends Controller
{
    protected $pagination_limit = 25;
    protected $_auth_user;
    protected $_site_profile;
    protected $error_route = 'admin.error';

    public function __construct()
    {

    }
    
    protected function loadDataToView($view_path)
    {
        view()->composer($view_path, function ($view) use ($view_path) {
            $view->with('view_path', $this->view_path);
            $view->with('trans_path', AppHelper::getTransPathFromViewPath($view_path));
            $view->with('base_route', $this->base_route);
            $view->with('_auth_user', auth()->user());

        });

        return $view_path;
    }




}