<?php
namespace App\Http\Controllers\Admin;


class DashboardController extends AdminBaseController
{
    protected $model;
    protected $base_route = 'admin.dashboard';
    protected $view_path = 'admin.dashboard';

    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data = [];


        return view(parent::loadDataToView($this->view_path.'.index'), compact('data'));
    }

    public function error500()
    {
        return view(parent::loadDataToView('admin.common.error-500'));
    }
}