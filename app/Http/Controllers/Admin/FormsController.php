<?php
namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\Question\QuestionAddValidationRequest;
use App\Http\Requests\Admin\Question\QuestionEditValidation;
use App\Model\Form;
use App\Model\Question;
use DB;
use Log;
use Illuminate\Http\Request;

class FormsController extends AdminBaseController
{
    protected $model;
    protected $base_route = 'admin.forms';
    protected $view_path = 'admin.form';
    protected $panel = 'form';

    public function index(Request $request)
    {
        $data = [];
        $data['rows']=Form::all();

        return view(parent::loadDataToView($this->view_path.'.index'), [
            'data' => $data,

        ]);
    }

    public function create()
    {
        $data=[];
        $data['questions']=Question::select('id','title')->where('status',1)->orderBy('title','asc')->get();

        return view(parent::loadDataToView($this->view_path.'.add'),['questions' => $data['questions']]);
    }

    public function store(QuestionAddValidationRequest $request)
    {
        try {

            $form = Form::create([
                'title' => $request->get('title'),
                'status' => $request->get('status'),
                'user_id'=> auth()->user()->id
            ]);


            $form_question = [];
            foreach ($request->get('question_id') as $key => $item) {

                $tmp = [];
                $tmp['form_id'] = $form->id;
                $tmp['question_id'] = $item;


                $form_question[] = $tmp;
            }



            foreach ($form_question as $item) {

                DB::table('form_questions')->insert([
                    'form_id' => $item['form_id'],
                    'question_id' => $item['question_id'],

                ]);

            }

            $request->session()->flash('curd_message', $this->panel . ' created successfully.');


        } catch (\Exception $e) {

            Log::error('Error generated : ' . $e->getMessage());
            $request->session()->flash('curd_message', 'Some error exist in code, contact you system admin.');

        }

        return redirect()->route($this->base_route . '.index');


    }


    public function edit($id)
    {
        if (!$this->model = Form::find($id)) {
            dd('Invalid form id passed.');
        }

        $data = [];
        $data['row'] = $this->model;
        $data['form_questions'] = $this->model->questions;
        $data['questions'] = Question::select('id', 'title')->where('status', 1)->orderBy('title', 'asc')->get();

        return view(parent::loadDataToView($this->view_path . '.edit'), [
            'data' => $data,
            'form_questions' => $data['form_questions'],
            'questions' => $data['questions']
        ]);

    }



    public function update(QuestionEditValidation $request, $id)
    {
        if (!$this->model = Form::find($id)) {
            dd('Invalid Form id passed.');
        }

        try {

            $this->model->title = $request->get('title');
            $this->model->inactive_message =$request->has('inactive_message')?$request->get('inactive_message'):'';
            $this->model->status = $request->get('status');
            $this->model->save();


            // Update Page Data
            $data = [];
            $data['form_question'] = [];
            $data['pivot_ids'] = [];
            foreach ($request->get('question_id') as $key => $item) {

                $tmp = [];
                $tmp['id'] = isset($request->get('pivot_table_id')[$key])?$request->get('pivot_table_id')[$key]:'';
                $data['pivot_ids'][] = $tmp['id'];

                $tmp['form_id'] = $this->model->id;
                $tmp['question_id'] = $item;
                $data['question_form'][] = $tmp;
            }
            // Remove deleted pages
            DB::table('form_questions')->where('form_id', $this->model->id)->whereNotIn('id', $data['pivot_ids'])->delete();

            foreach ($data['question_form'] as $item) {

                if ($item['id'] == '') {
                    DB::table('form_questions')->insert([
                        'form_id' => $item['form_id'],
                        'question_id' => $item['question_id'],

                    ]);
                } else {

                    DB::table('form_questions')->where('id', $item['id'])->update([
                        'form_id' => $item['form_id'],
                        'question_id' => $item['question_id'],
                    ]);

                }


            }

            $request->session()->flash('curd_message', $this->panel . ' Updated successfully.');


        } catch (\Exception $e) {

            Log::error('Error generated in Form Store Action: ' . $e->getMessage());
            $request->session()->flash('curd_message', 'Some error exist in code, contact you system admin.');

        }

        return redirect()->route($this->base_route . '.index');
    }

    public function destroy(Request $request, $id)
    {
        if (!$this->model = Form::find($id)) {
            return redirect()->route($this->error_route);
        }
        $this->model->delete();

        $request->session()->flash('curd_message', 'Questions deleted successfully.');
        return redirect()->route($this->base_route.'.index');

    }



    public function getQuestionHtml()
    {
        $data = [];
        $data['questions'] = Question::select('id', 'title')->where('status', 1)->orderBy('title', 'asc')->get();
        $response['error'] =false;
        $response['html'] = view(parent::loadDataToView($this->view_path .'.includes.questions-html'),[
            'questions' => $data['questions'],
        ])->render();

        return response()->json(json_encode($response));

    }



}