<?php
namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\Question\QuestionAddValidationRequest;
use App\Http\Requests\Admin\Question\QuestionEditValidation;
use App\Model\Question;
use DB;
use Illuminate\Http\Request;




class QuestionsController extends AdminBaseController
{
    protected $model;
    protected $base_route = 'admin.questions';
    protected $view_path = 'admin.questions';

    public function index(Request $request)
    {
        $data = [];
        $data['rows']=Question::all();

        return view(parent::loadDataToView($this->view_path.'.index'), [
            'data' => $data,

        ]);
    }

    public function create()
    {
        $data=[];

        $data['questions_type']=config('act360.form_notification.question.type');

        return view(parent::loadDataToView($this->view_path.'.add'),compact('data'));
    }

    public function store(QuestionAddValidationRequest $request)
    {
        $question = Question::create([
            'title' => $request->get('title'),
            'type' => $request->get('question-type'),
            'status' => $request->get('status'),
            'name' => $request->get('name'),
        ]);



        $request->session()->flash('curd_message', 'Question Added successfully.');
        return redirect()->route($this->base_route.'.index');


    }


    public function edit($id)
    {
        if (!$this->model = Question::find($id)) {
            return redirect()->route($this->error_route);
        }


        $data = [];
        $data['questions_type']=config('act360.form_notification.question.type');
        $data['row'] = $this->model;

        return view(parent::loadDataToView($this->view_path.'.edit'), [
            'data' => $data
        ]);

    }



    public function update(QuestionEditValidation $request, $id)
    {

        if (!$this->model = Question::find($id)) {
            dd('Invalid user id passed.');
        }

        $this->model->title = $request->get('title');
        $this->model->type = $request->get('question-type');
        $this->model->name = $request->get('name');
        $this->model->status = $request->get('status');

        $this->model->save();

        $request->session()->flash('curd_message', 'Questions updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function destroy(Request $request, $id)
    {
        if (!$this->model = Question::find($id)) {
            return redirect()->route($this->error_route);
        }
        $this->model->delete();

        $request->session()->flash('curd_message', 'Questions deleted successfully.');
        return redirect()->route($this->base_route.'.index');

    }




}