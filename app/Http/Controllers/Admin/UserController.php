<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\User\QuestionAddValidationRequest;
use App\Http\Requests\Admin\User\QuestionEditValidation;
use App\Http\Requests\Admin\User\UserAddValidationRequest;
use App\Model\UserDetails;
use App\User;
use DB;
use Illuminate\Http\Request;

class UserController extends AdminBaseController
{
    protected $model;
    protected $base_route = 'admin.users';
    protected $view_path = 'admin.user';

    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = User::select(
            'users.id', 'users.username', 'users.email',
            'users.updated_at', 'users.status',
            'ud.first_name', 'ud.middle_name', 'ud.last_name',
            DB::raw('CONCAT(ud.first_name, " ", ud.middle_name, " ", ud.last_name) AS full_name')
            )
            ->leftJoin('user_details as ud', 'users.id', '=', 'ud.user_id')
            ->where(function($query) use ($request) {
                if ($request->has('search_by')) {
                    $query->where($this->getSearchBy($request->get('search_by')), 'like', '%'.$this->getSearchKey($request->get('search_by'), $request->get('search_key')).'%');
                }

            })
            ->paginate($this->pagination_limit);



        $search_params = [];
        if ($request->has('search_by')) {
            $search_params['search_by'] = $request->get('search_by');
            $search_params['search_key'] = $request->get('search_key');
        }

        return view(parent::loadDataToView($this->view_path.'.index'), [
            'data' => $data,
            'search' => $search_params
        ]);
    }

    public function create()
    {
        return view(parent::loadDataToView($this->view_path.'.add'));
    }

    public function store(UserAddValidationRequest $request)
    {

        $user = User::create([
            'username' => $request->has('username')?$request->get('username'):'-',
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'status' => $request->get('status'),
        ]);

        if ($user) {

            UserDetails::create([
                'user_id' => $user->id,
                'first_name' => $request->get('first-name'),
                'middle_name' => $request->get('middle-name'),
                'last_name' => $request->get('last-name'),
                'gender' => $request->get('gender'),
                'address' => $request->get('address'),
                'phone' => $request->get('phone'),
            ]);

            $role_id=DB::table('roles')->select('id')->where('name',$request->get('role'))->first();
             DB::table('role_users')->insert([
                'user_id'=>$user->id,
                'role_id'=>$role_id->id

            ]);

        }


        $request->session()->flash('curd_message', 'User Added successfully.');
        return redirect()->route($this->base_route.'.index');


    }


    public function edit($user_id)
    {
        if (!$this->model = User::find($user_id)) {
            return redirect()->route($this->error_route);
        }

        $data = [];
        $data['row'] = $this->model;
        $data['user_details'] = $this->model->userDetails()->first();

        return view(parent::loadDataToView($this->view_path.'.edit'), [
            'data' => $data
        ]);

    }



    public function update(QuestionEditValidation $request, $user_id)
    {

        if (!$this->model = User::find($user_id)) {
            dd('Invalid user id passed.');
        }

        $this->model->username = $request->get('username');
        $this->model->email = $request->get('email');
        $this->model->status = $request->get('status');
        $this->model->is_admin = 1;

        if ($request->get('password') !== '')
            $this->model->password = $request->get('password');

        $this->model->save();

        if ($this->userModel = UserDetails::where('user_id', $this->model->id)->find($request->get('user_detail_id'))) {

            $this->userModel->update([
                'id' => $this->userModel->id,
                'first_name' => $request->get('first-name'),
                'middle_name' => $request->get('middle-name'),
                'last_name' => $request->get('last-name'),
                'address' => $request->get('address'),
                'phone' => $request->get('phone'),
                'gender' => $request->get('gender'),
            ]);


        }

        $request->session()->flash('curd_message', 'User updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function destroy(Request $request, $user_id)
    {
        if (!$this->model = User::find($user_id)) {
            return redirect()->route($this->error_route);
        }

        if (auth()->user()->id == $user_id) {
            return redirect()->route($this->error_route);
        }

        UserDetails::where('user_id', $this->model->id)->delete();
        $this->model->delete();

        $request->session()->flash('curd_message', 'User deleted successfully.');
        return redirect()->route($this->base_route.'.index');

    }




    /**
     * Helper Methods
     */


    /**
     * Returns Search Key For User Search
     *
     * @param $search_by
     * @return string
     */
    protected function getSearchBy($search_by)
    {
        switch ($search_by) {

            case 'id':
                return 'users.id';
                break;

            case 'first_name':
                return 'ud.first_name';
                break;

            case 'username':
                return 'users.username';
                break;

            case 'email':
                return 'users.email';
                break;

            case 'status':
                return 'users.status';
                break;

            case 'created_at':
                return 'users.created_at';
                break;

            default:
                return 'id';
        }
    }

    /**
     * Returns Search Key For User Search
     *
     * @param $search_by
     * @param $search_key
     * @return int|string
     */
    protected function getSearchKey($search_by, $search_key)
    {
        switch ($search_by) {

            case 'id':
            case 'first_name':
            case 'username':
            case 'email':
            case 'created_at':
                return $search_key;
                break;

            case 'status':
                $status = strtolower($search_key);
                if ($status == 'active')
                    return 1;

                return 0;
                break;

            default:
                return '';
        }
    }

}