<?php

namespace App\Http\Controllers\Front;


use App\Form\FormTrait;
use App\Http\Controllers\Controller;
use App\Model\Form;
use App\Notifications\FormSubmited;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use AppHelper;
use DB;
use Excel;

class HomeController extends Controller

{
    use FormTrait;
    protected $folder_path='front.home';
    protected $model;
    protected $base_route='home';

    public function __construct()
    {
    }

    public  function index(){

         $data=[];

         $data['form']=$this->getFormDetail();

         if ($data['form']){
         $data['form_question']=$this->getFormQuestionById($data['form']->id);
         }
        return view($this->folder_path.'.index',compact('data'));


   }


    public function formSubmit(Request $request){


       try {

           if ($file=$request->hasFile('attachment')){
               AppHelper::makeFolderIfNotExist($this->folder_path);



           $file_name = rand(5558, 9999) . '_' . $file->getClientOriginalName();
           }


           foreach($request->get('form_questions') as $key => $item){

               foreach ($request->except('_token','form_id','form_questions') as $form => $value){

               DB::table('form_responses')->insert([
                   'form_id'=>$request->get('form_id'),
                   'question_id'=>$item,
                   'form_responses'=>$value,
                   'created_at'=>Carbon::now()
               ]);
               }
           }
           $user_id=Form::select('user_id')->where('id',$request->get('form_id'))->first();
           $user=User::find($user_id->user_id);
           $user->notify(new FormSubmited($user));

           $request->session()->flash('curd_message','Form Submit Successfully');

       } catch (\Exception $e) {

           Log::error('Error generated Form Submitted Action: ' . $e->getMessage());
           $request->session()->flash('curd_message', 'Some error exist in code, contact you system admin.');

       }

       return redirect()->route($this->base_route . '.index');



   }


    public function exportFormResponse(){

       $form_response=$this->fromResponses()->get()->toArray();

        $form_response= json_decode( json_encode($form_response), true);

        Excel::create('form_response', function ($excel) use ($form_response) {

            $excel->sheet('Form Response', function ($sheet) use ($form_response) {
                $sheet->fromArray($form_response);
            });
        })->export('xls');





    }


}
