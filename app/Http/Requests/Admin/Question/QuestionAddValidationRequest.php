<?php

namespace App\Http\Requests\Admin\Question;

use App\Http\Requests\Request;

class QuestionAddValidationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',

        ];
    }


    public function messages()
    {
        return [
            'first-name.required' => 'Please, fill your first name.',
            'first-name.max' => 'First Name must be no more then 25.',
        ];
    }


}
