<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Request;

class UserAddValidationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first-name' => 'required|max:25',
            'middle-name' => 'max:25',
            'last-name' => 'required|max:25',
            'username' => 'required|max:255|unique:users,username',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|max:255|confirmed',
        ];
    }


    public function messages()
    {
        return [
            'first-name.required' => 'Please, fill your first name.',
            'first-name.max' => 'First Name must be no more then 25.',
        ];
    }


}
