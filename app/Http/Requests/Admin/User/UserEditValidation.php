<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Request;

class UserEditValidation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first-name' => 'required|max:25',
            'middle-name' => 'max:25',
            'last-name' => 'required|max:25',
            'username' => 'required|max:255|unique:users,username,'.$this->request->get('user_id'),
            'email' => 'required|email|max:255|unique:users,email,'.$this->request->get('user_id'),
            'password' => 'max:255|confirmed',
        ];
    }
}
