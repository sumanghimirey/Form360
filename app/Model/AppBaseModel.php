<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AppBaseModel extends Model
{
    /**
     * Set the status to boolean
     *
     * @param  string  $value
     * @return string
     */
    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value == 'active'?1:0;
    }

    /**
     * Scope a query to only include popular users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 1);
    }
    
}