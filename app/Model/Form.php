<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table = 'forms';

    protected $fillable = ['id','user_id', 'title','inactive_message','status'];


    public function questions(){
        return $this->belongsToMany(Question::class, 'form_questions', 'form_id',
            'question_id')->withPivot('id', 'form_id', 'question_id');
    }




}