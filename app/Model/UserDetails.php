<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    protected $table = 'user_details';

    protected $fillable = ['user_id', 'first_name','middle_name','last_name','gender','phone',
        'address','created_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}