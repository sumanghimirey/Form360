<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class CustomFacadeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('viewhelper', function()
        {
            return new \App\HelperClass\ViewHelper;
        });
        App::bind('apphelper', function()
        {
            return new \App\HelperClass\AppHelper;
        });

        App::bind('formhelper', function()
        {
            return new \App\HelperClass\FormHelper;
        });


    }
}
