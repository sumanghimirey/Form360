<?php

return [

    'site_configuration_keys' => [
        'admin_pagination_limit' => 'admin_pagination_limit',
    ],

    'asset' => [
        'path' => [
            'css' => 'front/assets/css/',
            'js' => 'front/assets/js/',
            'image' => 'images/'
        ],
    ],

    'form_notification'=>[
        'user'=>[
            'enable'=>'Enable',
            'disable'=>'Disable',
        ],

        'form-owner'=>[
            'enable'=>'Enable',
            'disable'=>'Disable',
        ],

        'question'=>[
            'type'=>[
            'checkbox'=>'Checkbox',
            'radio'   => 'Radio',
            'textarea' => 'TextArea',
            'textbox' =>  'TextBox',
            'attachment'=> 'Attachment'
            ],
            ],

    ]
];