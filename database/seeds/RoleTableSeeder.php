<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'admin',
            'slug' => 'admin',
            'status'=>1,
        ]);

        DB::table('users')->insert([
            'username' => 'form-owner',
            'slug' => 'form-owner',
            'status' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'user',
            'slug' => 'user',
            'status'=> 1,

            ]);

    }
}
