<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'act360',
            'email' => 'root@act360.com',
            'password' => bcrypt('root@act360'),
            'status'=>1,
            'is_admin'=>1
        ]);

        $user = DB::table('users')->first();

        DB::table('user_details')->insert([
            'user_id' => $user->id,
            'first_name' => 'act360',
            'middle_name' => '',
            'last_name' => '',
            'gender' => 'male',
            'address' => 'Kathmandu',
            'phone'=>'123456789'
        ]);
    }
}
