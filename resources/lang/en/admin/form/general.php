<?php
return [
    'page_title' => 'Form Manager',
    'manager' => 'Form Manager',
    'list' => 'Form List',
    'add-form' => 'Add Form',
    'title' => 'Title',
    'question-type' => 'Questions Type',
    'add' => 'Add',
    'phone' => 'phone',
    'add-questions' => 'Add',
    'name' => 'Name',
    'inactive_message' => 'InActive Message',
    'email' => 'Email',
    'update' => 'Updated At',
    'action' => 'Action',
    'question'=>'Questions',
    'id' => 'ID',
    'first-name' => 'First Name',
    'middle-name' => 'Middle Name',
    'update-form' => 'Update',
    'created_at' => 'Created Date',
    'email' => 'Email',
    'status' => 'Status',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'update-user' => 'Update',

    'menu' => [
        'list' => 'List',
        'add' => 'Add'
    ]
];