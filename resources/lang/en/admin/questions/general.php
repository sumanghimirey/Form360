<?php
return [
    'page_title' => 'Questions Manager',
    'manager' => 'Questions Manager',
    'list' => 'Questions List',
    'address' => 'Address',
    'title' => 'Title',
    'question-type' => 'Questions Type',
    'gender' => 'Gender',
    'phone' => 'phone',
    'add-questions' => 'Add',
    'name' => 'Name',
    'username' => 'User Name',
    'email' => 'Email',
    'update' => 'Updated At',
    'action' => 'Action',
    'id' => 'ID',
    'first-name' => 'First Name',
    'middle-name' => 'Middle Name',
    'update-form' => 'Update',
    'created_at' => 'Created Date',
    'email' => 'Email',
    'status' => 'Status',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'update-user' => 'Update',

    'menu' => [
        'list' => 'List',
        'add' => 'Add'
    ]
];