<?php
return [
    'page_title' => 'User Manager',
    'manager' => 'User Manager',
    'list' => 'User List',
    'address' => 'Address',
    'gender' => 'Gender',
    'phone' => 'phone',
    'name' => 'Name',
    'username' => 'User Name',
    'email' => 'Email',
    'update' => 'Updated At',
    'action' => 'Action',
    'add-user' => 'Add User',
    'add-form' => 'User Add Form',
    'update-user' => 'Edit User',
    'update-form' => 'Update Form',
    'id' => 'ID',
    'first-name' => 'First Name',
    'middle-name' => 'Middle Name',
    'last-name' => 'Last Name',
    'username' => 'User Name',
    'email' => 'Email',
    'address-1' => 'Address 1',
    'address-2' => 'Address 2',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirm',
    'new_password' => 'New Password',
    'new_password_confirmation' => 'Password Confirm',
    'status' => 'Status',
    'active' => 'Active',
    'inactive' => 'Inactive',

    'menu' => [
        'list' => 'List',
        'add' => 'Add'
    ]
];