<?php
return [
    'page_title' => 'Login Page - Act360',
    'company' => 'Act360',
    'login_request_message' => 'Please Enter Your Information',
    'remember_me' => 'Remember Me',
];