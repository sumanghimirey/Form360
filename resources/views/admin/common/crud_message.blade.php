@if(session()->has('curd_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        {{ session('curd_message') }}
        <br>
    </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <strong>Validation Failed !! </strong>
        Some field(s) failed validation. Please, find the highlighted fields below.
        <br>
    </div>
@endif
