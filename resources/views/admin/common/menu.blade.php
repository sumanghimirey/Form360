<div class="sidebar" id="sidebar" xmlns="http://www.w3.org/1999/html">
    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="icon-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="icon-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="icon-group"></i>
            </button>

            <button class="btn btn-danger">
                <i class="icon-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- #sidebar-shortcuts -->

    <ul class="nav nav-list">

        <li class="{{ Request::is('admin/dashboard')?'active':'' }}">
            <a href="{{ route(ViewHelper::getAdminRoute('dashboard')) }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>
                <li class="{{ Request::is('admin/users*')?'active open':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="icon-group"></i>
                <span class="menu-text"> {{ trans('admin/user/general.manager') }} </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="{{ Request::is('admin/users')?'active':'' }}">
                    <a href="{{ route(ViewHelper::getAdminRoute('users.index')) }}">
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/user/general.menu.list') }}
                    </a>
                </li>

                <li class="{{ Request::is('admin/users/create')?'active':'' }}">
                    <a href="{{ route(ViewHelper::getAdminRoute('users.create')) }}">
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/user/general.menu.add') }}
                    </a>
                </li>

            </ul>
        </li>

        <li class="{{ Request::is('admin/forms*')?'active open':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="icon-book"></i>
                <span class="menu-text"> {{ trans('admin/questions/general.manager') }} </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="{{ Request::is('admin/questions')?'active':'' }}">
                    <a href="{{ route(ViewHelper::getAdminRoute('questions.index')) }}">
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/questions/general.menu.list') }}
                    </a>
                </li>

                <li class="{{ Request::is('admin/questions/create')?'active':'' }}">
                    <a href="{{ route(ViewHelper::getAdminRoute('questions.create')) }}">
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/questions/general.menu.add') }}
                    </a>
                </li>

            </ul>
        </li>

        <li class="{{ Request::is('admin/forms*')?'active open':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="icon-code-fork"></i>
                <span class="menu-text"> {{ trans('admin/form/general.manager') }} </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="{{ Request::is('admin/forms')?'active':'' }}">
                    <a href="{{ route(ViewHelper::getAdminRoute('forms.index')) }}">
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/form/general.menu.list') }}
                    </a>
                </li>

                <li class="{{ Request::is('admin/forms/create')?'active':'' }}">
                    <a href="{{ route(ViewHelper::getAdminRoute('forms.create')) }}">
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/form/general.menu.add') }}
                    </a>
                </li>

            </ul>
        </li>





        <li class="">
            <form id="logout_form" action="{{ url('logout') }}" method="POST">
                {!! csrf_field() !!}
            </form>
            <a href="#">
                <i class="icon-off"></i>
                <span class="menu-text menu-logout" > Logout </span>
            </a>
        </li>



    </ul><!-- /.nav-list -->
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }
    </script>
</div>