@extends('admin.common.layout')

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('admin/dashboard/general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ trans($trans_path.'manager') }}</a>
                </li>
                <li class="active">{{ trans($trans_path.'update') }}</li>
            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ trans($trans_path.'manager') }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'update-form') }}
                    </small>

                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <form class="form-horizontal" role="form" action="{{ route($base_route.'.update', ['id' => $data['row']->id]) }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $data['row']->id }}">


                        @include($view_path.'.includes.form')

                        <hr>

                        @include($view_path.'.includes.questions')

                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="icon-ok bigger-110"></i>
                                    Update
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="icon-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div>
                        </div>

                        <div class="hr hr-24"></div>

                    </form>

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

@endsection
@section('page_specific_script')

    <!---- Inactive message attribute change script -->
    <script>

        $(function(){

            $(document).ready(function () {
                $('input:radio[name=status]').change(function () {
                    if ($("input[name='status']:checked").val() == 'active') {
                        $("#inactive_message").attr('disabled','disabled');
                    }
                    else{
                        $("#inactive_message").removeAttr('disabled');

                    }
                });
            });

            <!---- Question Html rendering  change script -->

            $('.add_field_button').click(function(e){

                // Jquery Ajax
                $.ajax({
                    url: '{{route($base_route.'.get-question-html')}}',
                    method:'GET',
                    success: function(response) {
                        var data= $.parseJSON(response);
                        $('#form_question_wrapper').append(data.html);
                    }
                });

                return false;
            });



        });

        function removeRow(node) {
            node.closest('td').closest('tr').remove();
        }





    </script>



@endsection