
<div class="space-4"></div>

<div class="{{ ViewHelper::ShowValidationClass($errors, 'title') }} form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'title') }} </label>

    <div class="col-sm-9">
        <input type="text" id="title" name="title" value="{{ ViewHelper::getFormValue('title', isset($data)?$data:'') }}" placeholder="{{ trans($trans_path.'title') }}" class="col-xs-10 col-sm-5" />
    </div>
    {{ ViewHelper::showValidationMessage($errors, 'title') }}
</div>

<div class="space-4"></div>
<div class="{{ ViewHelper::ShowValidationClass($errors, 'inactive_message') }} form-group ">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'inactive_message') }} </label>

    <div class="col-sm-9">
        <textarea rows="10" id="inactive_message" disabled name="inactive_message"  placeholder="{{ trans($trans_path.'inactive_message') }}" class="col-xs-10 col-sm-5" >

        {{ ViewHelper::getFormValue('inactive_message', isset($data)?$data:'') }}
        </textarea>
    </div>
    {{ ViewHelper::showValidationMessage($errors, 'position') }}
</div>

<div class="space-4"></div>


<div class="{{ ViewHelper::ShowValidationClass($errors, 'status') }} form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-5">{{ trans($trans_path.'status') }}</label>

    <div class="col-sm-9">
        <div class="control-group">
            <div class="radio">
                <label>
                    <input name="status" class="ace" type="radio" value="1" name="status" {{ ViewHelper::getFormStatus('active', isset($data)?$data:'') }} >
                    <span class="lbl"> {{ trans($trans_path.'active') }} </span>
                </label>
            </div>

            <div class="radio">
                <label>
                    <input name="status" class="ace" type="radio" value="0" name="status" {{ ViewHelper::getFormStatus('inactive', isset($data)?$data:'') }} >
                    <span class="lbl"> {{ trans($trans_path.'inactive') }} </span>
                </label>
            </div>

        </div>
        {{ ViewHelper::showValidationMessage($errors, 'status') }}
    </div>
</div>

