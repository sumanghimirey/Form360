<tr>
    <td>
        <select name="question_id[]" id="parent_id" class="form-control">
            <option value="0">Select Questions</option>
            @foreach($questions as $question)
                <option value="{{ $question->id }}">{{ $question->title }}</option>
            @endforeach
        </select>
    </td>

    <td>
        <span class="btn btn-sm btn-danger btn-recover" onclick="return removeRow(this);">
            <i class="icon icon-remove"></i>
        </span>
    </td>
</tr>