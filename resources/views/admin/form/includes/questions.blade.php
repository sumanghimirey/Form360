
<div class="table-responsive">
    <table id="sample-table-1" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>

            <th> {{ trans($trans_path.'question') }}</th>

            <th class="hidden-480"> Action</th>

        </tr>
        </thead>

        <tbody id="form_question_wrapper">

        @if (!isset($data))
        <tr>
            <td>
                <select name="question_id[]" id="parent_id" class="form-control">
                    <option value="0">Select Question</option>
                    @foreach($questions as $question)
                        <option value="{{ $question->id }}">{{ $question->title }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <span class="btn btn-sm btn-danger btn-recover" onclick="return removeRow(this);">
                    <i class="icon icon-remove"></i>
                </span>
            </td>
        </tr>
            @else

            @foreach($data['form_questions'] as $key => $form_question)
                <tr>
                <td>
                    <select name="question_id[]" id="parent_id" class="form-control">
                        <option value="0">select d</option>
                        @foreach($questions as $question)
                            <option value="{{ $question->id }}" {{ $form_question->pivot->question_id == $question->id?"selected":'' }}>{{ $question->title }}</option>
                        @endforeach
                    </select>

                    <!-- Pivot Table Ids -->
                    <input type="hidden" name="pivot_table_id[{{ $key }}]" value="{{ $form_question->pivot->id }}">


                </td>
                    <td>
                    <span class="btn btn-sm btn-danger btn-recover" onclick="return removeRow(this);">
                        <i class="icon icon-remove"></i>
                    </span>
                </td>
            </tr>
            @endforeach


        @endif
        </tbody>
        <tfoot>
        <tr>
            <td colspan="1">&nbsp;</td>
            <td colspan="1">
                <button type="button" class="btn btn-primary btn-recover add_field_button">
                    <i class="icon icon-plus"></i>
                </button>


            </td>
        </tr>
        </tfoot>
    </table>
</div>



