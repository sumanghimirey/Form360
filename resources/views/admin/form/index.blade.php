@extends('admin.common.layout')


@section('page_specific_css')

    <style>

        .main-content {
            background-color:  red;
        }


    </style>

    @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('admin/dashboard/general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ trans($trans_path.'manager') }}</a>
                </li>
                <li class="active">{{ trans($trans_path.'list') }}</li>
            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ trans($trans_path.'manager') }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'list') }}
                    </small>

                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    @include('admin.common.crud_message')

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            <label>
                                                <input type="checkbox" class="ace" />
                                                <span class="lbl"></span>
                                            </label>
                                        </th>
                                        <th> title</th>
                                        <th>created at</th>
                                        <th>status</th>
                                        <th class="hidden-480">action</th>

                                    </tr>
                                    </thead>

                                    <tbody>

                                    @if($data['rows']->count() > 0)
                                        @foreach($data['rows'] as $row)
                                            <tr>
                                            <td class="center">
                                                <label>
                                                    <input type="checkbox" class="ace" />
                                                    <span class="lbl"></span>
                                                </label>
                                            </td>
                                            <td>
                                               {{$row->title}}

                                            </td>
                                                <td>
                                                    {{$row->created_at}}

                                                </td>
                                                <td><span class="label label-sm label-{{ $row->status == 1?'success':'warning' }}">{{ $row->status == 1?'Active':'Inactive' }}</span></td>
                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                                                    <a href="{{ route($base_route.'.edit', [ 'id' => $row->id ]) }}" class="btn btn-xs btn-info">
                                                        <i class="icon-edit bigger-120"></i>

                                                    </a>

                                                    <a href="{{ route($base_route.'.destroy', [ 'id' => $row->id ]) }}"  class="btn btn-xs btn-danger bootbox-confirm">
                                                        <i class="icon-trash bigger-120"></i>
                                                    </a>

                                                </div>

                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8"> No Data Found.</td>
                                        </tr>
                                    @endif

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                    <div class="hr hr-18 dotted hr-double"></div>

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

    @endsection

