@extends('admin.common.layout')

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('admin/dashboard/general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route('admin.users.index') }}">{{ trans($trans_path.'manager') }}</a>
                </li>
                <li class="active">{{ trans($trans_path.'update-user') }}</li>
            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ trans($trans_path.'manager') }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'update-form') }}
                    </small>

                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    @include('admin.common.crud_message')

                    <form class="form-horizontal" role="form" action="{{ route(ViewHelper::getAdminRoute('questions.update'), ['id' => $data['row']->id]) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="question_id" value="{{ $data['row']->id }}">


                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'title') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'title') }} <span class="required-field">*</span> </label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="title" value="{{$data['row']?$data['row']->title: old('title') }}" placeholder="{{ trans($trans_path.'title') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'title') }}
                        </div>

                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'name') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Input Name <span class="required-field">*</span> </label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="name" value="{{$data['row']?$data['row']->name: old('title') }}" placeholder="Eg.Email" class="col-xs-10 col-sm-5" />
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'title') }}
                        </div>


                        <div class="space-4"></div>


                        <div class=" form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'question-type') }} </label>

                            <div class="col-sm-4">
                                <select name="question-type"  class="form-control col-xs-4 col-sm-5" >{{ old('question-type') }}>
                                    @foreach($data['questions_type'] as $key => $value)
                                        <option value="{{$key}}"> {{$value}} </option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="space-4"></div>


                        <hr>








                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'status') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-5">{{ trans($trans_path.'status') }}</label>

                            <?php
                                $chk_1 = 'checked';
                                $chk_2 = '';

                                if ($data['row']->status == 0) {
                                    $chk_1 = '';
                                    $chk_2 = 'checked';
                                }

                                if (old('status')) {

                                    if (old('status') == 1) {

                                        $chk_1 = '';
                                        $chk_2 = 'checked';

                                    } else {

                                        $chk_1 = 'checked';
                                        $chk_2 = '';

                                    }



                                }


                            ?>


                            <div class="col-sm-9">
                                <div class="control-group">
                                    <div class="radio">
                                        <label>
                                            <input name="status" class="ace" type="radio" value="1" name="status" {{ $chk_1 }} >
                                            <span class="lbl"> {{ trans($trans_path.'active') }} </span>
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label>
                                            <input name="status" class="ace" type="radio" value="0" name="status" {{ $chk_2 }}>
                                            <span class="lbl"> {{ trans($trans_path.'inactive') }} </span>
                                        </label>
                                    </div>

                                </div>
                                {{ ViewHelper::showValidationMessage($errors, 'status') }}
                            </div>
                        </div>

                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="icon-ok bigger-110"></i>
                                    Submit
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="icon-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div>
                        </div>

                        <div class="hr hr-24"></div>

                    </form>

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

    @endsection