@extends('admin.common.layout')

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('admin/dashboard/general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route('admin.users.index') }}">{{ trans($trans_path.'manager') }}</a>
                </li>
                <li class="active">{{ trans($trans_path.'update-user') }}</li>
            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ trans($trans_path.'manager') }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'update-form') }}
                    </small>

                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    @include('admin.common.crud_message')

                    <form class="form-horizontal" role="form" action="{{ route(ViewHelper::getAdminRoute('users.update'), ['id' => $data['row']->id]) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{ $data['row']->id }}">
                        <input type="hidden" name="user_detail_id" value="{{ $data['user_details']->id }}">
                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'first-name') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'first-name') }} <span class="required-field">*</span></label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="first-name" value="{{ old('first-name')?old('first-name'):$data['user_details']->first_name }}" placeholder="{{ trans($trans_path.'first-name') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'first-name') }}
                        </div>

                        <div class="space-4"></div>

                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'middle-name') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'middle-name') }} </label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="middle-name" value="{{ old('middle-name')?old('middle-name'):$data['user_details']->middle_name }}" placeholder="{{ trans($trans_path.'middle-name') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'middle-name') }}
                        </div>

                        <div class="space-4"></div>

                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'last-name') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'last-name') }} <span class="required-field">*</span></label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="last-name" value="{{ old('last-name')?old('last-name'):$data['user_details']->last_name }}" placeholder="{{ trans($trans_path.'last-name') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'last-name') }}
                        </div>

                        <div class="space-4"></div>

                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'username') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'username') }} <span class="required-field">*</span></label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="username" value="{{ old('username')?old('username'):$data['row']->username }}" placeholder="{{ trans($trans_path.'username') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'username') }}
                        </div>

                        <div class="space-4"></div>

                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'email') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'email') }} <span class="required-field">*</span></label>

                            <div class="col-sm-9">
                                <input type="email" id="form-field-1" name="email" value="{{ old('email')?old('email'):$data['row']->email }}" placeholder="{{ trans($trans_path.'email') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'email') }}
                        </div>

                        <div class="space-4"></div>

                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'address') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'address') }} </label>

                            <div class="col-sm-9">
                                <textarea name="address" id="" rows="3" class="form-control col-xs-10 col-sm-5" >{{ old('address')?old('address'):$data['user_details']->address }}</textarea>
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'address') }}
                        </div>

                        <div class="space-4"></div>

                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'phone') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'phone') }} </label>

                            <div class="col-sm-9">
                                <input type="text" name="phone" value="{{ old('phone')?old('phone'):$data['user_details']->phone }}" class="form-control col-xs-10 col-sm-5" >
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'phone') }}
                        </div>


                        <div class=" form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'gender') }} </label>

                            <div class="col-sm-9">
                                <select name="gender"  class="form-control col-xs-10 col-sm-5" {{ old('gender') }} >
                                    <option value="male"> Male </option>
                                    <option value="female"> Female </option>
                                </select>
                            </div>

                        </div>>

                        <div class="space-4"></div>





                        <hr>

                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'password') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'new_password') }} </label>

                            <div class="col-sm-9">
                                <input type="password" id="form-field-1" name="password" placeholder="{{ trans($trans_path.'new_password') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'password') }}
                        </div>

                        <div class="space-4"></div>

                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'password_confirmation') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans($trans_path.'new_password_confirmation') }} </label>

                            <div class="col-sm-9">
                                <input type="password" id="form-field-1" name="password_confirmation" placeholder="{{ trans($trans_path.'new_password_confirmation') }}" class="col-xs-10 col-sm-5" />
                            </div>
                            {{ ViewHelper::showValidationMessage($errors, 'password_confirmation') }}
                        </div>

                        <div class="space-4"></div>


                        <hr>








                        <div class="{{ ViewHelper::ShowValidationClass($errors, 'status') }} form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-5">{{ trans($trans_path.'status') }}</label>

                            <?php
                                $chk_1 = 'checked';
                                $chk_2 = '';

                                if ($data['row']->status == 0) {
                                    $chk_1 = '';
                                    $chk_2 = 'checked';
                                }

                                if (old('status')) {

                                    if (old('status') == 'inactive') {

                                        $chk_1 = '';
                                        $chk_2 = 'checked';

                                    } else {

                                        $chk_1 = 'checked';
                                        $chk_2 = '';

                                    }



                                }


                            ?>


                            <div class="col-sm-9">
                                <div class="control-group">
                                    <div class="radio">
                                        <label>
                                            <input name="status" class="ace" type="radio" value="active" name="status" {{ $chk_1 }} >
                                            <span class="lbl"> {{ trans($trans_path.'active') }} </span>
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label>
                                            <input name="status" class="ace" type="radio" value="inactive" name="status" {{ $chk_2 }}>
                                            <span class="lbl"> {{ trans($trans_path.'inactive') }} </span>
                                        </label>
                                    </div>

                                </div>
                                {{ ViewHelper::showValidationMessage($errors, 'status') }}
                            </div>
                        </div>

                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="icon-ok bigger-110"></i>
                                    Submit
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="icon-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div>
                        </div>

                        <div class="hr hr-24"></div>

                    </form>

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

    @endsection