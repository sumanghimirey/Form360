@extends('admin.common.layout')


@section('page_specific_css')

    <style>

        .main-content {
            background-color: red;
        }


    </style>

@endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('admin/dashboard/general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route('admin.users.index') }}">{{ trans($trans_path.'manager') }}</a>
                </li>
                <li class="active">{{ trans($trans_path.'list') }}</li>
            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off"/>
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ trans($trans_path.'manager') }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'list') }}
                    </small>

                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    @include('admin.common.crud_message')

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th colspan="5">
                                            <select name="search_by" id="search_by">
                                                <option value="id" {{ Request::get('search_by') == 'id'?'selected':'' }}>User Id</option>
                                                <option value="first_name" {{ Request::get('search_by') == 'first_name'?'selected':'' }}>First Name</option>
                                                <option value="username" {{ Request::get('search_by') == 'username'?'selected':'' }}>Name</option>
                                                <option value="email" {{ Request::get('search_by') == 'email'?'selected':'' }}>Email</option>
                                                <option value="status" {{ Request::get('search_by') == 'status'?'selected':'' }}>Status</option>
                                                <option value="created_at" {{ Request::get('search_by') == 'created_at'?'selected':'' }}>Created At</option>
                                            </select>
                                            <input type="text" name="search_key" id="search_key" value="{{ Request::get('search_key')?Request::get('search_key'):'' }}">
                                            <button id="search_btn" class="btn btn-xs btn-info">Search</button>
                                            <p id="search_by_hint" style="display: none;"></p>
                                        </th>
                                        <th colspan="2"></th>
                                    </tr>
                                    <tr>
                                        <th> {{ trans($trans_path.'id') }}</th>
                                        <th> {{ trans($trans_path.'name') }}</th>
                                        <th> {{ trans($trans_path.'username') }}</th>
                                        <th> {{ trans($trans_path.'email') }}</th>
                                        <th>
                                            <i class="icon-time bigger-110 hidden-480"></i>
                                            {{ trans($trans_path.'update') }}
                                        </th>
                                        <th>{{ trans($trans_path.'status') }}</th>
                                        <th class="hidden-480"> {{ trans($trans_path.'action') }}</th>

                                    </tr>
                                    </thead>

                                    <tbody>

                                    @if ($data['rows']->count() > 0)
                                        @foreach($data['rows'] as $row)
                                            <tr>
                                                <td>{{ $row->id}}</td>
                                                <td>{{ $row->full_name }}</td>
                                                <td>{{ $row->name }}</td>
                                                <td>{{ $row->email }}</td>
                                                <td>{{ $row->updated_at }}</td>
                                                <td><span class="label label-sm label-{{ $row->status == 1?'success':'warning' }}">{{ $row->status == 1?'Active':'Inactive' }}</span></td>
                                                <td>
                                                    <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                                                        <a href="{{ route('admin.users.edit', [ 'id' => $row->id ]) }}" class="btn btn-xs btn-info">
                                                            <i class="icon-edit bigger-120"></i>
                                                        </a>

                                                        <a href="{{ route('admin.users.destroy', [ 'id' => $row->id ]) }}" class="btn btn-xs btn-danger bootbox-confirm">
                                                            <i class="icon-trash bigger-120"></i>
                                                        </a>

                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach

                                    @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                    @endif

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="7">
                                            {!! $data['rows']->appends($search)->render() !!}
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                    <div class="hr hr-18 dotted hr-double"></div>

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

@endsection


@section('page_specific_script')

    <script>
        $(document).ready(function () {

            $('#search_btn').click(function () {

                var search_by = $('#search_by').val();
                var search_key = $('#search_key').val();

                var current_url = '{{ URL::current() }}';

                var redirect_url = current_url + '?search_by=' + search_by + '&search_key=' + search_key;
                location.href = redirect_url;

                // console.log(redirect_url);
            });


            $('#search_by').on('change', function () {
                if ($(this).val() == 'status') {
                    $('#search_by_hint').html('Possible keys are [ active / inactive ]').show();
                } else {
                    $('#search_by_hint').hide();
                }
            });


        });
    </script>

@endsection