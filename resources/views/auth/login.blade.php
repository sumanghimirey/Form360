<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from 198.74.61.72/themes/preview/ace/login.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 28 Feb 2014 17:46:42 GMT -->
<head>
    <meta charset="utf-8"/>
    <title>{!! trans('auth/login/general.page_title') !!}</title>

    <meta name="description" content="User login page"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- basic styles -->

    <link href="{{ asset('admin-template/assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('admin-template/assets/css/font-awesome.min.css')  }}"/>

    <!--[if IE 7]>
    <link rel="stylesheet" href="{{ asset('admin-template/assets/css/font-awesome-ie7.min.css') }}"/>
    <![endif]-->

    <!-- page specific plugin styles -->

    <!-- fonts -->

    <link rel="stylesheet" href="{{ asset('admin-template/assets/css/ace-fonts.css')  }}"/>

    <!-- ace styles -->

    <link rel="stylesheet" href="{{ asset('admin-template/assets/css/ace.min.css')  }}"/>
    <link rel="stylesheet" href="{{ asset('admin-template/assets/css/ace-rtl.min.css')  }}"/>

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="{{ asset('admin-template/assets/css/ace-ie.min.css') }}"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    <script src="{{ asset('admin-template/assets/js/html5shiv.js') }}"></script>
    <script src="{{ asset('admin-template/assets/js/respond.min.js') }}"></script>
    <![endif]-->
</head>

<body class="login-layout">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="center">
                        <h1>
                            <i class="icon-leaf green"></i>
                            <span class="red">Sign In</span>
                        </h1>
                        <h4 class="blue">&copy; {!! trans('auth/login/general.company') !!}</h4>
                    </div>

                    <div class="space-6"></div>

                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header blue lighter bigger">
                                        <i class="icon-coffee green"></i>
                                       {{ trans('auth/login/general.login_request_message') }}
                                    </h4>

                                    <div class="space-6"></div>

                                    <form role="form" method="POST" action="{{ url('/login') }}">
                                        {{ csrf_field() }}
                                        <fieldset>
                                            <label class="block clearfix {{ $errors->has('email') ? ' has-error' : '' }}">
														<span class="block input-icon input-icon-right">
                                                             <input id="email" type="email" class="form-control" name="email" placeholder="email" value="{{ old('email') }}">

															<i class="icon-user"></i>
														</span>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                @endif
                                            </label>

                                            <label class="block clearfix {{ $errors->has('email') ? ' has-error' : '' }}">
														<span class="block input-icon input-icon-right">
															<input id="password" type="password" class="form-control" name="password" placeholder="password">
															<i class="icon-lock"></i>
														</span>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                @endif
                                            </label>

                                            <div class="space"></div>

                                            <div class="clearfix">
                                                <label class="inline">
                                                    <input type="checkbox" name="remember" class="ace"/>
                                                    <span class="lbl"> {!! trans('auth/login/general.remember_me') !!}</span>
                                                </label>

                                                <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                    <i class="icon-key"></i>
                                                    Login
                                                </button>
                                            </div>

                                            <div class="space-4"></div>
                                        </fieldset>
                                    </form>

                                </div><!-- /widget-main -->

                            </div><!-- /widget-body -->
                        </div><!-- /login-box -->
                    </div><!-- /position-relative -->
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->

<script type="text/javascript">
    window.jQuery || document.write("<script src='{{ asset('admin-template/assets/js/jquery-2.0.3.min.js') }}'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='{{ asset('admin-template/assets/js/jquery-1.10.2.min.js') }}'>" + "<" + "/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='{{ asset('admin-template/assets/js/jquery.mobile.custom.min.js') }}'>" + "<" + "/script>");
</script>

<!-- inline scripts related to this page -->

<script type="text/javascript">
    function show_box(id) {
        jQuery('.widget-box.visible').removeClass('visible');
        jQuery('#' + id).addClass('visible');
    }
</script>
</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/login.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 28 Feb 2014 17:46:42 GMT -->
</html>
