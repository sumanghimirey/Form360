<footer class="lfooter">
    <div class="container">
        <div class="footer-content">
            <div class="row">
                <div class="col-md-5 col-sm-4">
                    <div class="foot-box">
                        <h6>Quick Links</h6>
                        <ul>
                            <li><a href="#">Who We Are?</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 col-sm-4">
                    <div class="foot-box l-box">
                        <a href=""> Do Awesoe Thing</a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="foot-box">
                        <h6>Follow Us</h6>
                        <div class="social clearfix">
                            <ul class="floated-list clearfix">
                                <li>
                                    <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <span>Copyright &copy; 2017 Act360 .</span>
        </div>
    </div>
</footer>
<!-- js library -->
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/library.min.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/front/js/slick.min.js')}}"></script>

<!-- bootstrap js -->
<script type="text/javascript" src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>

<!-- scrollbar js -->
<script type="text/javascript" src="{{asset('assets/front/js/scrollbar.js')}}"></script>

<!-- Custom Js -->
<script type="text/javascript" src="{{asset('assets/front/js/custom.js')}}"></script>

@yield('js')