<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Act 360 Form</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />


    <!-- font icons css -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/font-awesome.css') }}">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/bootstrap.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/scrollbar.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/slick.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/slick-theme.css') }}"/>

    <!-- custom css -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/style.css') }}">

    @yield('css')

</head>