<header class="header clearfix">
    <div class="container">
        <div class="mytable">
            <div class="logo table-cell va-middle">
                <a href="{{route('home.index')}}">
                    Act 360 Form
                </a>
            </div>
            <div class="my-menu table-cell va-middle">
                <ul class="floated-list clearfix">
                    <li class="active"><a href="#">360 Act Form</a></li>

                </ul>
            </div>
        </div>
        <div class="toggle">
            <a href="">
                <img src="{{asset('assets/front/images/toggle.png')}}">
            </a>
        </div>
    </div>
</header>