
@extends('front.layout.front_master')

@section('content')

   <div class="row">

       <div class="col-md-9 col-md-offset-2">
           @include('admin.common.crud_message')
           @if(!isset($data['form']))

               <label class="alert alert-danger col-md-9">
                   No Form is Activate this time

               </label>
            @else

               @if(isset($data['form_question']) && $data['form_question']->count()>0 )
                   <button type="button" id="embedded_form" class="btn btn-success pull-right"> Embedded This Form </button>
               <br> <br> <br>
               <div  class="embededarea pull-right col-lg-4" style="display: none;">


<textarea rows="10" cols="20">

                    <iframe src="{{url('')}}" style="border:1px #000000 solid;" name="" scrolling="auto" align="bottom" height="500" width="1200">

                   </iframe>
</textarea>




               </div>
           <form id="inquiryForm" method="post" action="{{route('front.form.submit')}}">
                <input type="hidden" value="{{$data['form']->id}}" name="form_id">
               <div id="content-alert"></div>
               <label class="form-title alert alert-success">
                   {{$data['form']->title?$data['form']->title:'No title of this Form'}} </label>
               {{csrf_field()}}

               @if(isset($data['form_question']))

                @foreach($data['form_question'] as $form_question)
                    <input type="hidden" value="{{$form_question->id}}" name="form_questions[]">

                   {!!FormHelper::renderFormByType($form_question->title,$form_question->type,$form_question->name)!!}

                   @endforeach

               @else
                   <label class="alert alert-danger col-md-9">
                       No Question of this Form

                   </label>
               @endif



               <button type="submit" class="btn btn-default">Submit</button>
           </form>

               @endif
               @endif

       </div>
   </div>
   <br>

@section('js')
    <script src="{{asset('assets/front/js/jquery.validate.min.js')}}"> </script>
    <script>

        $(function(){
            $(document).ready(function () {
                $("#inquiryForm").validate({
                    errorElement: "div",
                    errorPlacement: function (error, element) {
                        error.appendTo("div#content-alert").addClass('alert alert-danger');
                    },
                    rules: {
                        name: {
                            required: true,
                            minlength: 6,
                            alpha: true,
                        },
                    },
                    messages: {
                        name: {
                            required: "requied message",
                            minlength: "6 character"
                        },

                    },
                    submitHandler: function (form) {
                        $('#loading').show();
                        form.submit();
                    }
                });

            });

            $('#embedded_form').click(function(e){
                $('.embededarea').show();

            });




        });
    </script>

    @endsection

@endsection