<!DOCTYPE html>
<html>
@include('front.common.head')
<body>

<div class="body-inactive fixed cover"></div>
<div class="offcanvas">
    <div class="coff absolute">
        <a href="">
            <img src="{{asset('assets/front/images/csb.png')}}" width="22">
        </a>
    </div>
    <div class="nano">
        <div class="nano-content">
            <!-- Dynamic Cloing of Navigation For Mobile -->
        </div>
    </div>
</div>

<!-- Header Markup -->
@include('front.common.header')

<!-- Main body Markup -->
<main>

@yield('content')

</main>

<!-- Footer Markup -->
@include('front.common.footer')



</body>
</html>
