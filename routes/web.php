<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::auth();

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin\\'], function () {


    Route::get('dashboard',           ['as' => 'dashboard',                     'uses' => 'DashboardController@index']);
    Route::get('error',               ['as' => 'error',                         'uses' => 'DashboardController@error500']);

    /*
     * Users route
     */
    Route::get('users',               ['as' => 'users.index',                   'uses' => 'UserController@index']);
    Route::get('users/create',        ['as' => 'users.create',                  'uses' => 'UserController@create']);
    Route::post('users',              ['as' => 'users.store',                   'uses' => 'UserController@store']);
    Route::get('users/{id}',          ['as' => 'users.show',                    'uses' => 'UserController@show']);
    Route::get('users/{id}/edit',     ['as' => 'users.edit',                    'uses' => 'UserController@edit']);
    Route::post('users/{id}',         ['as' => 'users.update',                  'uses' => 'UserController@update']);
    Route::get('users/{id}/destroy',  ['as' => 'users.destroy',                 'uses' => 'UserController@destroy']);


    /*
    *Questions Route
    */

    Route::get('questions',               ['as' => 'questions.index',        'uses' => 'QuestionsController@index']);
    Route::get('questions/create',        ['as' => 'questions.create',       'uses' => 'QuestionsController@create']);
    Route::post('questions',              ['as' => 'questions.store',        'uses' => 'QuestionsController@store']);
    Route::get('questions/{id}',          ['as' => 'questions.show',         'uses' => 'QuestionsController@show']);
    Route::get('questions/{id}/edit',     ['as' => 'questions.edit',         'uses' => 'QuestionsController@edit']);
    Route::post('questions/{id}',         ['as' => 'questions.update',       'uses' => 'QuestionsController@update']);
    Route::get('questions/{id}/destroy',  ['as' => 'questions.destroy',      'uses' => 'QuestionsController@destroy']);

/*
 * form route
 */
    Route::get('forms',               ['as' => 'forms.index',               'uses' => 'FormsController@index']);
    Route::get('forms/create',        ['as' => 'forms.create',              'uses' => 'FormsController@create']);
    Route::post('forms',              ['as' => 'forms.store',               'uses' => 'FormsController@store']);
    Route::get('forms/get-form-html',['as' =>'forms.get-question-html',  'uses' => 'FormsController@getQuestionHtml']);
    Route::get('forms/{id}',          ['as' => 'forms.show',                'uses' => 'FormsController@show']);
    Route::get('forms/{id}/edit',     ['as' => 'forms.edit',                'uses' => 'FormsController@edit']);
    Route::post('forms/{id}',         ['as' => 'forms.update',              'uses' => 'FormsController@update']);
    Route::get('forms/{id}/destroy',  ['as' => 'forms.destroy',             'uses' => 'FormsController@destroy']);
    Route::get('forms/{id}/destroy',  ['as' => 'forms.destroy',             'uses' => 'FormsController@destroy']);


});

Route::group(['namespace' => 'Front\\'], function () {

    Route::get('/', ['as' => 'home.index', 'uses' => 'HomeController@index']);

    Route::post('form-submit', ['as' => 'front.form.submit', 'uses' => 'HomeController@formSubmit']);

    Route::get('forms/response',  ['as' => 'forms.response',             'uses' => 'HomeController@exportFormResponse']);




});

